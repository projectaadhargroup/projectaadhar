(function () {
  'use strict';

  var app=angular
    .module('aadharGroupApp', [
      //'aadharGroupApp.config',

      //'ngAnimate',
	  'aadharGroupApp.routes',
      'aadharGroupApp.index'
      
    ]);

  angular.module('aadharGroupApp.config', []);

  angular.module('aadharGroupApp.routes', ['ngRoute', 'ui.router']).run(function($rootScope) {
    /*$rootScope.static_path = 'https://healthbrio.s3.amazonaws.com/';*/
    $rootScope.static_path = 'http://127.0.0.1:8000/static/';
  });

})();






