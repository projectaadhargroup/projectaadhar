(function () {
  'use strict';

  angular
    .module('aadharGroupApp.config')
    .config(config);

  config.$inject = ['$locationProvider'];


   /** @name config
   * @desc Enable HTML5 routing*/

  function config($locationProvider) {
    $locationProvider.html5Mode({
      enabled: false,
      requireBase: false
    });
    $locationProvider.hashPrefix('!');
  }
})();
