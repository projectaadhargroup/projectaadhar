(function () {
  'use strict';

  angular
    .module('aadharGroupApp.routes')
    .config(indexConfig);

  indexConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
  
/**
   * @name config
   * @desc Define valid application routes*/

   //***  stateProvider for Symptoms*****
  window.static_path = 'http://127.0.0.1:8000/static/';
  /*window.static_path = 'https://healthbrio.s3.amazonaws.com/';*/

  function indexConfig($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise('/home');
    $stateProvider
        .state('home', {
            url: '/home',
            controller:'HomeController',
            controllerAs:'home_vm',
            templateUrl: static_path+'templates/home/home.html'
        })
  }


})();