from django.shortcuts import render
from .models import *
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
import json
from django.core.mail import send_mail
from Crypto.Cipher import AES
import random
from django.http import HttpResponseRedirect, HttpResponse
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.authentication import *
from rest_framework.permissions import *
from rest_framework.response import Response
from rest_framework import status
from oauth2_provider.settings import oauth2_settings
from oauthlib.common import generate_token
from django.http import JsonResponse
from django.utils.timezone import now, timedelta
from django.shortcuts import render
from rest_framework.generics import CreateAPIView,ListAPIView
from rest_framework import generics
from oauth2_provider.models import AccessToken, Application, RefreshToken
import requests
from authentication.serializers import *
from rest_framework import status
import binascii
DOMAIN_NAME='http://127.0.0.1:8000/'

def get_token_json(acces_token):

    token = {
        'acces_token' : acces_token.token,
        'expires_in' : acces_token.expires,
        'refresh_token' : acces_token.refresh_token.token,
        'scope' : acces_token.scope
    }

    return token

def get_access_token(user):


    app = Application.objects.get(user=user)
    token = generate_token()

    refresh_token = generate_token()
    # print type(timedelta(seconds=oauth2_settings.ACCESS_TOKEN_EXPIRE_SECONDS))
    expires = now()+timedelta(seconds=oauth2_settings.ACCESS_TOKEN_EXPIRE_SECONDS)
    # expires = str(timedelta(seconds=oauth2_settings.ACCESS_TOKEN_EXPIRE_SECONDS))
    scope = "read write"

    acces_token = AccessToken.objects.\
        create(user = user,
                application = app,
                expires = expires,
                token = token,
                scope = scope)

    RefreshToken.objects.\
        create(user = user,
                application = app,
                token = refresh_token,
                access_token = acces_token)

    return get_token_json(acces_token)



def sendmail(subject,body,to):
	print "send"
	send_mail(subject, body, settings.EMAIL_HOST_USER,to, fail_silently=False)


def email_verification(profile):
    try:
        profile=Account.objects.get(username=profile.username)
        profile.key=str(random.randint(100,999)) 
        profile.user_joining_time=timezone.now()
        profile.save()

        key =settings.KEY_ID
        username_plaintext =profile.username
        length = 16 - (len(username_plaintext) % 16)
        username_plaintext += ("#")*length

        encobj = AES.new(key, AES.MODE_ECB)
        username_ciphertext = encobj.encrypt(username_plaintext)

        key_plaintext =profile.key
        length = 16 - (len(key_plaintext) % 16)
        key_plaintext += ("#")*length
        key_encobj = AES.new(key, AES.MODE_ECB)
        key_ciphertext = key_encobj.encrypt(key_plaintext)

        email_subject = 'Client Email Varification'

        email_body = "Hey,thanks for signing up. To activate your account, click this link "+DOMAIN_NAME+username_ciphertext.encode('hex')+"/"+key_ciphertext.encode('hex')+"/confirm/"
        sendmail(email_subject,email_body,[profile.email])

    except Exception, e:
        print e



class SignUp(generics.CreateAPIView):
    queryset = Account.objects.all()
    serializer_class = SignUpSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        print self.request.user
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        try:
            instance=Account.objects.get(email=request.data['email'])
            instance.set_password(request.data['password'])
            instance.signup_source_local=True
            instance.save()
            email_verification(instance)
            print "down"
            Application.objects.create(user=instance,client_type=Application.CLIENT_CONFIDENTIAL,authorization_grant_type=Application.GRANT_PASSWORD)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

        except Exception,e:
            print e
            return Response(status=status.HTTP_400_BAD_REQUEST)


def email_confirm(request,username,key_data):

    try:
        key = settings.KEY_ID
        user_ciphertext = binascii.unhexlify(username)
        decobj = AES.new(key, AES.MODE_ECB)
        user_plaintext = decobj.decrypt(user_ciphertext)
        key_ciphertext = binascii.unhexlify(key_data)
        decobj = AES.new(key, AES.MODE_ECB)
        key_plaintext = decobj.decrypt(key_ciphertext)
        u = Account.objects.get(username=(user_plaintext.strip('#')))
        if int(u.key)==int(key_plaintext.strip('#')):
        	u.is_active=True
        	u.save()

        	return HttpResponse('Done')

        else:
        	return HttpResponse('Error')
    
    except Exception ,e:
    	return HttpResponse(e)



class Login(generics.ListAPIView):
    queryset = Account.objects.all()
    serializer_class = LoginSerializer
    authentication_class = (BasicAuthentication,)

    def get_queryset(self):
    	print "hii",self.request.user
    	return [self.request.user]



class Token_confirm(generics.ListAPIView):

    def post(self,request):

        try:

            # obj=literal_eval(request.body)
            # # print  request.POST.get['email','']
            # print obj['email']
            # email = obj['email']
            # print "second enter"
            # access_token = obj['access_token']
            # print "third enter"
            # print type(request.user)
            # print type(email)

            # authentication_classes = (BasicAuthentication,) 

            if str(request.user) == email:
                if Account.objects.filter(email=email,is_active=True).exists():
                    user_id = Account.objects.get(email = email)
                    expires = AccessToken.objects.get(user = user_id,token = access_token)

                    if expires > now:
                        return Response("Tokwn is not valid, Please login again")

                    elif expires < now:
                        queryset = AccessToken.objects.get(token=access_token)
                        serializer = Token_confirm_Serializer(queryset)
                        return Response(serializer.data)

            else:
                print "error"

                return Response(status=status.HTTP_401_UNAUTHORIZED)
        except Exception ,e:

            return HttpResponse(e)



