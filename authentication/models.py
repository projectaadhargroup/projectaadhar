from django.contrib.auth.models import AbstractBaseUser
from django.db import models
from django.contrib.auth.models import *
from django.contrib.auth import *
from django.db import models
from oauth2_provider.models import Application





class AccountManager(BaseUserManager):
    def create_user(self, email, password=None, **kwargs):
        print "hiii"
        if not email:
            raise ValueError('Users must have a valid email address.')

        if not kwargs.get('username'):
            raise ValueError('Users must have a valid username.')

        account = self.model(
            email=self.normalize_email(email), username=kwargs.get('username')
        )

        account.set_password(password)
        account.save()
        Application.objects.create(user=account, client_type=Application.CLIENT_CONFIDENTIAL,authorization_grant_type=Application.GRANT_PASSWORD)
        return account

    def create_superuser(self, email, password, **kwargs):
        account = self.create_user(email, password, **kwargs)

        account.is_admin = True
        account.is_staff= True
        account.is_superuser=True
        account.is_active=True
        account.save()

        return account



class Account(AbstractBaseUser,PermissionsMixin):
    email = models.EmailField(unique=True)
    username = models.CharField(max_length=90, unique=True)

    first_name = models.CharField(max_length=90, blank=True,null=True)
    middle_name = models.CharField(max_length=90,blank=True,null=True)    
    last_name = models.CharField(max_length=90, blank=True,null=True)
    mobile = models.CharField(max_length=50, blank=True,null=True)
    key=models.CharField(max_length = 128,blank=True,null=True)    

    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    # signup_source_facebook = models.BooleanField(default=False)
    # signup_source_google = models.BooleanField(default=False)
    # signup_source_local = models.BooleanField(default=False)


    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = AccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __unicode__(self):
        return self.email

    def get_full_name(self):
        return ' '.join([self.first_name, self.last_name])

    def get_short_name(self):
        return self.first_name