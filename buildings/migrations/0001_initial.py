# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Building',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, null=True, blank=True)),
                ('slug', models.SlugField(max_length=150, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Building_group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, null=True, blank=True)),
                ('slug', models.SlugField(max_length=250, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Flat_group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, null=True, blank=True)),
                ('slug', models.SlugField(max_length=150, null=True, blank=True)),
                ('area', models.CharField(max_length=100, null=True, blank=True)),
                ('price', models.CharField(max_length=100, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Floor_group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, null=True, blank=True)),
                ('slug', models.SlugField(max_length=150, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('building', models.ForeignKey(related_name='building', blank=True, to='buildings.Building', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Floors',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, null=True, blank=True)),
                ('slug', models.SlugField(max_length=150, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('floor_group', models.ForeignKey(related_name='floor_group', blank=True, to='buildings.Floor_group', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Images',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('coordinates', models.CharField(max_length=500, null=True, blank=True)),
                ('url', models.ImageField(null=True, upload_to=b'media', blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='floor_group',
            name='detailed_image',
            field=models.OneToOneField(related_name='detailed_image_floor_group', null=True, blank=True, to='buildings.Images'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='floor_group',
            name='overview_image',
            field=models.OneToOneField(related_name='overview_image_floor_group', null=True, blank=True, to='buildings.Images'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='flat_group',
            name='floors',
            field=models.ForeignKey(related_name='floors', blank=True, to='buildings.Floors', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='building',
            name='building_group',
            field=models.ForeignKey(related_name='building_group', blank=True, to='buildings.Building_group', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='building',
            name='detailed_image',
            field=models.OneToOneField(related_name='building_detailed_image', null=True, blank=True, to='buildings.Images'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='building',
            name='overview_image',
            field=models.OneToOneField(related_name='building_overview_image', null=True, blank=True, to='buildings.Images'),
            preserve_default=True,
        ),
    ]
