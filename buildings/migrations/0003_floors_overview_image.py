# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('buildings', '0002_floors_detailed_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='floors',
            name='overview_image',
            field=models.OneToOneField(related_name='overview_image_floor', null=True, blank=True, to='buildings.Images'),
            preserve_default=True,
        ),
    ]
