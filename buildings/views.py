from django.shortcuts import render
from .models import *
from rest_framework import generics
from django.views.generic import View
from rest_framework.views import APIView
from .serializers import *



class Building_group_View(generics.ListAPIView):
	serializer_class = Building_Serializer

	def get_queryset(self):
		return Building.objects.all()



class Building_View(generics.ListAPIView):
	serializer_class = Floor_group_Serializer
	

	def get_queryset(self):
		name = self.kwargs['name']
		print name
		building = Building.objects.get(slug=name)
		return Floor_group.objects.filter(building=building)



class Floor_group_View(generics.ListAPIView):
	serializer_class = Floors_Serializer
	

	def get_queryset(self):
		name = self.kwargs['name']
		print name
		floor_group = Floor_group.objects.get(slug=name)
		return Floors.objects.filter(floor_group=floor_group)


class Floors_View(generics.ListAPIView):
	serializer_class = Flat_group_Serializer
	

	def get_queryset(self):
		name = self.kwargs['name']
		print name
		floors = Floors.objects.get(slug=name)
		return Flat_group.objects.filter(floors=floors)
