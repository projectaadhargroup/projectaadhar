from django.contrib import admin
from .models import *

# Register your models here.
class Entry(admin.ModelAdmin):
	prepopulated_fields = {'slug': ('name', )}
	list_display = ('name','slug')


admin.site.register(Images)
admin.site.register(Building)
admin.site.register(Building_group,Entry)
admin.site.register(Floor_group)
admin.site.register(Floors)
admin.site.register(Flat_group)


