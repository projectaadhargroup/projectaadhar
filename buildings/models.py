from django.db import models
from django.template.defaultfilters import slugify
# Create your models here.



class Images(models.Model):
	# name = models.CharField(max_length=100,blank=True,null=True)
	coordinates = models.CharField(max_length=500,blank=True,null=True)
	url = models.ImageField(upload_to='media',blank=True,null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.coordinates 





class Building_group(models.Model):
	name = models.CharField(max_length=200,blank=True,null=True)
	slug = models.SlugField(max_length=250,blank=True,null=True)
	# building_name = models.CharField(max_length=200,blank=True,null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name


	def save(self, *args, **kwargs):
		self.slug = slugify(self.name)
		super(Building_group, self).save(*args, **kwargs)




class Building(models.Model):
	name = models.CharField(max_length=100,blank=True,null=True)
	slug = models.SlugField(max_length=150,blank=True,null=True)
	building_group = models.ForeignKey(Building_group,blank=True,null=True,related_name="building_group")
	overview_image = models.OneToOneField(Images,blank=True,null=True,related_name="building_overview_image")
	detailed_image = models.OneToOneField(Images,blank=True,null=True,related_name="building_detailed_image")
	# floor_group_name = models.CharField(max_length=200,blank=True,null=True)
	# floor_group = models.CharField(max_length=100,blank=True,null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	# AVAIL_CHOICES = (
	# 	('AVAILABLE', 'AVAILABLE'),
	# 	('SOLD', 'SOLD'),
	# 	('ON HOLD', 'ON HOLD'),
	# 	)
	# availibility = models.CharField(max_length=10, choices=AVAIL_CHOICES)

	def save(self, *args, **kwargs):
		self.slug = slugify(self.name)
		super(Building, self).save(*args, **kwargs)


	def __str__(self):
		return self.slug 





class Floor_group(models.Model):
	name = models.CharField(max_length=100,blank=True,null=True)
	slug = models.SlugField(max_length=150,blank=True,null=True)
	building = models.ForeignKey(Building,blank=True,null=True,related_name="building")
	# floors = models.CharField(max_length=200,blank=True,null=True)
	# available = models.IntegerField(blank=True,null=True)
	# sold = models.IntegerField(blank=True,null=True)
	overview_image = models.OneToOneField(Images,blank=True,null=True,related_name="overview_image_floor_group")
	detailed_image = models.OneToOneField(Images,blank=True,null=True,related_name="detailed_image_floor_group")
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def save(self, *args, **kwargs):
		self.slug = slugify(self.name)
		super(Floor_group, self).save(*args, **kwargs)

	def __str__(self):
		return self.slug
		
class Floors(models.Model):
	name = models.CharField(max_length=100,blank=True,null=True)
	slug = models.SlugField(max_length=150,blank=True,null=True)
	floor_group = models.ForeignKey(Floor_group,blank=True,null=True,related_name='floor_group')
	# flat_group = models.CharField(max_length=100,blank=True,null=True)
	# floor_type = models.CharField(max_length=100,blank=True,null=True)
	# available = models.IntegerField(blank=True,null=True)
	# sold = models.IntegerField(blank=True,null=True)
	overview_image = models.OneToOneField(Images,blank=True,null=True,related_name="overview_image_floor")
	detailed_image = models.OneToOneField(Images,blank=True,null=True,related_name="detailed_image_floor")
	# flat_group = models.ManyToManyField(Flat_group,blank=True,null=True,related_name="Flat_group")
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	
	def save(self, *args, **kwargs):
		self.slug = slugify(self.name)
		super(Floors, self).save(*args, **kwargs)

	def __str__(self):
		return self.slug 



class Flat_group(models.Model):
	name = models.CharField(max_length=100,blank=True,null=True)
	slug = models.SlugField(max_length=150,blank=True,null=True)
	# AVAIL_CHOICES = (
	# 	('AVAILABLE', 'AVAILABLE'),
	# 	('SOLD', 'SOLD'),
	# 	('ON HOLD', 'ON HOLD'),
	# 	)
	# availibility = models.CharField(max_length=10, choices=AVAIL_CHOICES)
	floors = models.ForeignKey(Floors,blank=True,null=True,related_name="floors")
	area = models.CharField(max_length=100,blank=True,null=True)
	price = models.CharField(max_length=100,blank=True,null=True)
	# overview_image = models.OneToOneField(Images,blank=True,null=True,related_name="overview_image_flat_group")
	# flat_name = models.CharField(max_length=100,blank=True,null=True)
	# detailed_image = models.OneToOneField(Images,blank=True,null=True,related_name="detailed_image_flat_group")
	# coordinates = models.CharField(max_length=500,blank=True,null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def save(self, *args, **kwargs):
		self.slug = slugify(self.name)
		super(Flat_group, self).save(*args, **kwargs)


	def __str__(self):
		return self.slug 
