from django.contrib.auth.models import User, Group
from rest_framework import serializers
# from .models import *
from flat.models import Flat
from buildings.models import Building_group,Building,Floor_group,Floors,Flat_group



class Building_group_Serializer(serializers.ModelSerializer):


    class Meta:
        model = Building_group
        fields = ('name','slug',)



class Building_Serializer(serializers.ModelSerializer):
    building_group = Building_group_Serializer()
    flats_available = serializers.SerializerMethodField()
    flats_on_hold = serializers.SerializerMethodField()
    flats_sold = serializers.SerializerMethodField()

    class Meta:
        model = Building
        fields = ('name','slug','overview_image','detailed_image','building_group','flats_available','flats_on_hold','flats_sold')

    def get_flats_available(self,obj):
      print "hiiiiiiiii"
      building = Building.objects.all()
      floor_group = Floor_group.objects.filter(building=building)
      floor = Floor_group.objects.filter(floor_group=floor_group)
      flat = Flat.objects.filter(floor=floor,availibility="AVAILABLE")
      return {'available_flats':flat.count(),}

    def get_flats_on_hold(self,obj):
      flat = Flat.objects.filter(availibility="ON HOLD")
      return {'on_hold_flats':flat.count(),}

    def get_flats_sold(self,obj):
      flat = Flat.objects.filter(availibility="SOLD")
      return {'sold_flats':flat.count(),}


class Building1_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Building
        fields = ('name','slug','detailed_image',)

class Floor_group_Serializer(serializers.ModelSerializer):

    building = Building1_Serializer()
    flats_available = serializers.SerializerMethodField()
    flats_on_hold = serializers.SerializerMethodField()
    flats_sold = serializers.SerializerMethodField()

    class Meta:
        model = Floor_group
        fields = ('name','slug','overview_image','detailed_image','building','flats_available','flats_on_hold','flats_sold')

    def get_flats_available(self,obj):
      floor_group = Floor_group.objects.filter(name=obj)
      floor = Floor_group.objects.filter(floor_group=floor_group)
      flat = Flat.objects.filter(floor=floor,availibility="AVAILABLE")
      return flat.count()

    def get_flats_on_hold(self,obj):
      floor_group = Floor_group.objects.filter(name=obj)
      floor = Floor_group.objects.filter(floor_group=floor_group)
      flat = Flat.objects.filter(floor=floor,availibility="ON HOLD")
      return flat.count()

    def get_flats_sold(self,obj):
      floor_group = Floor_group.objects.filter(name=obj)
      floor = Floor_group.objects.filter(floor_group=floor_group)
      flat = Flat.objects.filter(floor=floor,availibility="SOLD")
      return flat.count()



class Floor1_group_Serializer(serializers.ModelSerializer):

    class Meta:
        model = Floor_group
        fields = ('name','slug','detailed_image',)


class Floors_Serializer(serializers.ModelSerializer):
    floor_group = Floor1_group_Serializer()
    flats_available = serializers.SerializerMethodField()
    flats_on_hold = serializers.SerializerMethodField()
    flats_sold = serializers.SerializerMethodField()

    class Meta:
        model = Floors
        fields = ('name','slug','overview_image','detailed_image','floor_group','flats_available','flats_on_hold','flats_sold')

    def get_flats_available(self,obj):
      floor = Floor_group.objects.filter(floor_group=obj)
      flat = Flat.objects.filter(floor=floor,availibility="AVAILABLE")
      return flat.count()

    def get_flats_on_hold(self,obj):
      floor = Floor_group.objects.filter(floor_group=obj)
      flat = Flat.objects.filter(floor=floor,availibility="ON HOLD")
      return flat.count()

    def get_flats_sold(self,obj):
      floor = Floor_group.objects.filter(floor_group=obj)
      flat = Flat.objects.filter(floor=floor,availibility="SOLD")
      return flat.count()




class Floors1_Serializer(serializers.ModelSerializer):

    class Meta:
        model = Floors
        fields = ('name','slug','detailed_image')


class Flat_group_Serializer(serializers.ModelSerializer):
    floors = Floors1_Serializer()
    class Meta:
        model = Flat_group
        fields = ('name','slug','floors',)