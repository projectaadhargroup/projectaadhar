from django.contrib import admin
from flat.models import *

admin.site.register(Flat_2D_Image)
admin.site.register(Flat_3D_Image)
admin.site.register(Flat)
admin.site.register(Flat_Details)
admin.site.register(Flat_Amenities)
admin.site.register(Society_Amenities)
admin.site.register(Price_Breakup)
admin.site.register(Payment_Plan_Breakdown)
admin.site.register(Terms)
# admin.site.register(Payment_Plans)
# Register your models here.
