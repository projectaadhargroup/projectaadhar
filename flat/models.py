from django.db import models
from buildings.models import *

class Flat_2D_Image(models.Model):
	image_id = models.CharField(max_length=20)
	image_2d = models.ImageField(upload_to='static',blank=True,null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
 
class Flat_3D_Image(models.Model):
	image_id = models.CharField(max_length=20)
	image_3d = models.ImageField(upload_to='static',blank=True,null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)


class Flat(models.Model):
	booking_price = models.FloatField()
	flat_name = models.CharField(max_length=50)
	flat_type = models.CharField(max_length=50)
	flat_size =  models.CharField(max_length=50)
	floor_2d_image = models.OneToOneField(Flat_2D_Image)
	floor_3d_image = models.OneToOneField(Flat_3D_Image)
	floor = models.ForeignKey(Floors)
	AVAIL_CHOICES = (
		('AVAILABLE', 'AVAILABLE'),
		('SOLD', 'SOLD'),
		('ON HOLD', 'ON HOLD'),
	)
	availibility = models.CharField(max_length=10, choices=AVAIL_CHOICES)
	flat_price = models.CharField(max_length=50)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return str(self.booking_price)


class Flat_Details(models.Model):
	no_of_balcony = models.IntegerField(default=0)
	no_of_bathroom = models.IntegerField(default=0)

	def __str__(self):
		return "Flat_Detail"

class Flat_Amenities(models.Model):
	is_Intercom = models.BooleanField(default=None)
	is_Washing_Area = models.BooleanField(default=None)
	is_Power_Backup = models.BooleanField(default=None)
	is_Gas_Line = models.BooleanField(default=None)
	is_AC = models.BooleanField(default=None)
	is_Cupboards = models.BooleanField(default=None)
	is_Home_Automation = models.BooleanField(default=None)
	is_Living_Room = models.BooleanField(default=None)
	is_Maid_Room = models.BooleanField(default=None)
	is_Kitchen = models.BooleanField(default=None)
	is_Pooja_Room = models.BooleanField(default=None)
	is_Study_Room = models.BooleanField(default=None)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return "Flat Eminity"


class Society_Amenities(models.Model):
	is_Lift = models.BooleanField(default=None)
	is_Security = models.BooleanField(default=None)
	is_Swimming_Pool = models.BooleanField(default=None)
	is_Gymnasium = models.BooleanField(default=None)
	is_Garden = models.BooleanField(default=None)
	is_Library = models.BooleanField(default=None)
	is_Community_Hall = models.BooleanField(default=None)
	is_Internal_Roads = models.BooleanField(default=None)
	is_Jogging_Track = models.BooleanField(default=None)
	is_Internet = models.BooleanField(default=None)
	is_Play_Area = models.BooleanField(default=None)
	is_Power_Backup = models.BooleanField(default=None)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
			return "Society Amenities"


class Price_Breakup(models.Model):
	overall_discounted_price = models.CharField(max_length=30)
	other_charges = models.CharField(max_length=30)
	club_house_charges = models.CharField(max_length=30)
	development_charges = models.CharField(max_length=30)
	vat = models.CharField(max_length=30)
	registration_charges = models.CharField(max_length=30)
	service_tax = models.CharField(max_length=30)
	total_price = models.CharField(max_length=30)
	


class Payment_Plan_Breakdown(models.Model):
	On_booking = models.CharField(max_length=30)
	on_agreement = models.CharField(max_length=30)
	other_charges_on_agreement = models.CharField(max_length=30)
	plinth_time_charge = models.CharField(max_length=30)
	on_1st_slab = models.CharField(max_length=30)
	on_2nd_slab = models.CharField(max_length=30)
	on_3rd_slab = models.CharField(max_length=30)
	on_4th_slab = models.CharField(max_length=30) 
	on_roof_slab = models.CharField(max_length=30)
	on_flooring_work = models.CharField(max_length=30)
	on_possession = models.CharField(max_length=30)

class Terms(models.Model):
	value = models.TextField(max_length=500)


