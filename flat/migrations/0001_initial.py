# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('buildings', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Flat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('booking_price', models.FloatField()),
                ('flat_name', models.CharField(max_length=50)),
                ('flat_type', models.CharField(max_length=50)),
                ('flat_size', models.CharField(max_length=50)),
                ('availibility', models.CharField(max_length=10, choices=[(b'AVAILABLE', b'AVAILABLE'), (b'SOLD', b'SOLD'), (b'ON HOLD', b'ON HOLD')])),
                ('flat_price', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('floor', models.ForeignKey(to='buildings.Floors')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Flat_2D_Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image_id', models.CharField(max_length=20)),
                ('image_2d', models.ImageField(null=True, upload_to=b'static', blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Flat_3D_Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image_id', models.CharField(max_length=20)),
                ('image_3d', models.ImageField(null=True, upload_to=b'static', blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Flat_Amenities',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_Intercom', models.BooleanField(default=None)),
                ('is_Washing_Area', models.BooleanField(default=None)),
                ('is_Power_Backup', models.BooleanField(default=None)),
                ('is_Gas_Line', models.BooleanField(default=None)),
                ('is_AC', models.BooleanField(default=None)),
                ('is_Cupboards', models.BooleanField(default=None)),
                ('is_Home_Automation', models.BooleanField(default=None)),
                ('is_Living_Room', models.BooleanField(default=None)),
                ('is_Maid_Room', models.BooleanField(default=None)),
                ('is_Kitchen', models.BooleanField(default=None)),
                ('is_Pooja_Room', models.BooleanField(default=None)),
                ('is_Study_Room', models.BooleanField(default=None)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Flat_Details',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('no_of_balcony', models.IntegerField(default=0)),
                ('no_of_bathroom', models.IntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Payment_Plan_Breakdown',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('On_booking', models.CharField(max_length=30)),
                ('on_agreement', models.CharField(max_length=30)),
                ('other_charges_on_agreement', models.CharField(max_length=30)),
                ('plinth_time_charge', models.CharField(max_length=30)),
                ('on_1st_slab', models.CharField(max_length=30)),
                ('on_2nd_slab', models.CharField(max_length=30)),
                ('on_3rd_slab', models.CharField(max_length=30)),
                ('on_4th_slab', models.CharField(max_length=30)),
                ('on_roof_slab', models.CharField(max_length=30)),
                ('on_flooring_work', models.CharField(max_length=30)),
                ('on_possession', models.CharField(max_length=30)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Price_Breakup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('overall_discounted_price', models.CharField(max_length=30)),
                ('other_charges', models.CharField(max_length=30)),
                ('club_house_charges', models.CharField(max_length=30)),
                ('development_charges', models.CharField(max_length=30)),
                ('vat', models.CharField(max_length=30)),
                ('registration_charges', models.CharField(max_length=30)),
                ('service_tax', models.CharField(max_length=30)),
                ('total_price', models.CharField(max_length=30)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Society_Amenities',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_Lift', models.BooleanField(default=None)),
                ('is_Security', models.BooleanField(default=None)),
                ('is_Swimming_Pool', models.BooleanField(default=None)),
                ('is_Gymnasium', models.BooleanField(default=None)),
                ('is_Garden', models.BooleanField(default=None)),
                ('is_Library', models.BooleanField(default=None)),
                ('is_Community_Hall', models.BooleanField(default=None)),
                ('is_Internal_Roads', models.BooleanField(default=None)),
                ('is_Jogging_Track', models.BooleanField(default=None)),
                ('is_Internet', models.BooleanField(default=None)),
                ('is_Play_Area', models.BooleanField(default=None)),
                ('is_Power_Backup', models.BooleanField(default=None)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Terms',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.TextField(max_length=500)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='flat',
            name='floor_2d_image',
            field=models.OneToOneField(to='flat.Flat_2D_Image'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='flat',
            name='floor_3d_image',
            field=models.OneToOneField(to='flat.Flat_3D_Image'),
            preserve_default=True,
        ),
    ]
