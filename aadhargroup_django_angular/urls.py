from django.conf.urls import patterns, include, url
from django.contrib import admin
from authentication import views as v1
from buildings import views as v2

urlpatterns = patterns('',
    # url(r'^', include(router.urls)),
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^signup/$', v1.SignUp.as_view(), name="sign_up"),
    url(r'^login/$', v1.Login.as_view(), name="login"),
    url(r'^token_confirm/$', v1.Token_confirm.as_view(), name="token_confirm"),
    url(r'^(?P<username>\w+)/(?P<key_data>\w+)/confirm/$', v1.email_confirm),
    url(r'^building_group/$', v2.Building_group_View.as_view()),
    url(r'^building_group/(?P<name>[-\w]+)', v2.Building_View.as_view()),
    url(r'^floor_group/(?P<name>[-\w]+)', v2.Floor_group_View.as_view()),
    url(r'^floors/(?P<name>[-\w]+)', v2.Floors_View.as_view()),
)
