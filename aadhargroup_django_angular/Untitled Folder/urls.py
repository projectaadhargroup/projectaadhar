from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import patterns, url,include
from buildings import views

from django.conf.urls import url, include





#router = routers.DefaultRouter()
#router.register(r'symptoms', views.main_symptomViewSet)
#router.register(r'cause', views.CausesViewSet)


urlpatterns = patterns('',
    url(r'^building_group/$', views.Building_group_View.as_view()),
    url(r'^building_group/(?P<name>[-\w]+)', views.Building_View.as_view()),
    url(r'^floor_group/(?P<name>[-\w]+)', views.Floor_group_View.as_view()),
    url(r'^floors/(?P<name>[-\w]+)', views.Floors_View.as_view()),

)
